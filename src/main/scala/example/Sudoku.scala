package example

import scala.io.Source

sealed trait Cell
case class Fixed(num: Int) extends Cell
case class Possible(nums: List[Int]) extends Cell

object Sudoku extends App {
  type Row = List[Cell]
  type Grid = List[Row]

  val sampleGridStr1 = ".......1.4.........2...........5.4.7..8...3....1.9....3..4..2...5.1........8.6..."
  val sampleGridStr2 = "6......1.4.........2...........5.4.7..8...3....1.9....3..4..2...5.1........8.6..."
  val sampleGridStr3 = ".......21.8.3........4.9...1...6..5..3....4......2....2...7.......8..9..5........"

  def readCell(c: Char): Option[Cell] = c match {
    case '.' => Some(Possible(Range(1,10).toList))
    case c if c.isDigit => Some(Fixed(c.asDigit))
    case _ => None
  }

  def testReadCell() = {
    println(readCell('a'))
    println(readCell('1'))
    println(readCell('.'))
    println(readCell('9'))
  }

  def traverser[A, B](list: List[A])(f: A => Option[B]): Option[List[B]] = {
    def inner[A](elem: Option[A], list: Option[List[A]]): Option[List[A]] = {
      for { l <- list; e <- elem } yield e :: l
    }

    list.foldRight[Option[List[B]]](Some(Nil))( (elem, list) => inner(f(elem), list) )
  }

  def sequencer[A](list: List[Option[A]]) = traverser(list)(identity)

  def readRow(s: String): Option[Row] = {
    sequencer[Cell](s.map(readCell).toList)
  }

  def testReadRow() = {
    println(readRow("123"))
    println(readRow("foo"))
    println(readRow("8.6.21"))
  }

  def readGrid(str: String): Option[Grid] = {
    sequencer[Row](str.grouped(9).toList.map(readRow).toList)
  }

  def testReadGrid() = {
    println(readGrid("123456789.1.2.3.4."))
  }

  def pruneCellsOld(cells: List[Cell]): Option[List[Cell]] = {
    val fixeds = cells.collect { case Fixed(x) => x }

    def pruneCell(c: Cell): Option[Cell] =  c match {
      case Possible(nums) => nums.filterNot(fixeds.toSet) match {
        case Nil => None
        case single :: Nil => Some(Fixed(single))
        case many => Some(Possible(many))
      }
      case f @ Fixed(_) => Some(f)
    }

    traverser(cells)(pruneCell)
  }

  def testPruneCells() = {
    println(pruneCells(List(Fixed(4), Possible(Range(3,8).toList), Fixed(6))))  // sub-range.
    println(pruneCells(List(Fixed(4), Fixed(5), Fixed(6))))                     // no change.
    println(pruneCells(List(Fixed(3), Fixed(8), Possible(List(3,8)))))          // nothing left.  
    println(pruneCells(List(Fixed(3), Fixed(5), Possible(Range(3,6).toList))))  // all singles.
  }

  def showGrid(grid: Grid) = {
    def showRow(row: Row) = {
      for (cell <- row) {
        cell match {
          case Fixed(fv) => print(s" $fv ")
          case Possible(_) => print(" . ")
        }
      }
      println()
    }

    for (row <- grid) showRow(row)
  }

  def showRow(row: Row) = {
    def printPossibilities(nums: List[Int]) = {
      print("[ ")
      Range(0,10).map { n =>
        nums.contains(n) match {
          case true => print(n)
          case false => print(" ")
        }
      }
      print(" ]")
    }

    for (cell <- row) {
      cell match {
        case Fixed(fv) => print(s"      $fv       ")
        case Possible(pv) => printPossibilities(pv)
      }
    }
    println()
  }

  def showGridWithPossibilities(grid: Grid) = {
    for (row <- grid) showRow(row)
  }

  def subGridToRows(grid: Grid): Grid = {
    grid
      .grouped(3)
      .toList
      .map(rows => rows
        .map(row => row
          .grouped(3)
          .toList))
      .map(group => group
        .transpose
        .map(x => x.flatten))
      .flatten
  }

  def testSubGridToRows() = {
    val Some(aGrid) = readGrid(sampleGridStr1)

    val subGridView = subGridToRows(aGrid)
    assert(subGridView(0)(0) == aGrid(0)(0))
    assert(subGridView(0)(1) == aGrid(0)(1))

    assert(subGridView(0)(3) == aGrid(1)(0))
    assert(subGridView(0)(5) == aGrid(1)(2))

    assert(subGridView(0)(6) == aGrid(2)(0))
    assert(subGridView(0)(8) == aGrid(2)(2))

    val subgridAgain = subGridToRows(subGridView)
    assert(aGrid == subgridAgain)
  }

  def pruneGrid(grid: Grid): Option[Grid] = {
    //println("Pruning grid ...")
    //showGridWithPossibilities(grid)
    for {
      prunedRows <- traverser(grid)(pruneCells)
      prunedColumns <- traverser(prunedRows.transpose)(pruneCells)
      prunedSubGrids <- traverser(subGridToRows(prunedColumns.transpose))(pruneCells)
    } yield subGridToRows(prunedSubGrids)
  }

  def testPruneGrid() = {
    val Some(aGrid) = readGrid(sampleGridStr2)
    showGridWithPossibilities(aGrid)

    println("\n\nAfter pruning \n\n")

    val Some(prunedGrid) = pruneGrid(aGrid)
    showGridWithPossibilities(prunedGrid)
  }

  // Debugging-helper, remove later.
  def optionShow[T](v: Option[T]): String = v match {
    case None => "NONE"
    case Some(_) => "SOME"
  }

  def fixM[T](f: T => Option[T], x: T): Option[T] = {
    for {
      next_x <- f(x)
      answer <- if (next_x == x) Some(x) else fixM(f, next_x)
    } yield answer
  }

  def fixPoint(grid: Grid): Option[Grid] = {
    fixM[Grid](pruneGrid, grid)
  }

  def testFixPoint() = {
    val Some(aGrid) = readGrid(sampleGridStr2)
    showGridWithPossibilities(aGrid)

    println("\n\nAfter pruning \n\n")

    fixPoint(aGrid) match {
      case Some(prunedGrid) => showGridWithPossibilities(prunedGrid)
      case None => println("No solution found !!")
    }
  }

  // Helpers for nextGrids
  def isPossible(c: Cell) = c match {
    case Possible(_) => true
    case Fixed(_) => false
  }

  def possibilityCount(c: Cell) = c match {
    case Fixed(_) => 1
    case Possible(nums) => nums.length
  }

  def fixCell(i: Int, c: Cell) = c match {
    case Possible(x :: y :: Nil) => (i, Fixed(x), Fixed(y))
    case Possible(x :: rest) => (i, Fixed(x), Possible(rest))
    case _ => throw new IllegalArgumentException(s"Impossible situation! ${c}")
  }

  def testFixCell() = {
    println(fixCell(4, Possible(List(1,2,4))))
    println(fixCell(4, Possible(List(1,5))))
  }

  def replace[T](p: Int, f: T => T, elems: List[T]): List[T] = {
    // Apply `f` to the p'th element.
    for {
      (x, i) <- elems.zipWithIndex
    } yield {
      if (i == p)
        f(x)
      else
        x
    }
  }

  def replace2D(i: Int, v: Cell, g: Grid): Grid = {
    // Replace the i'th cell in the grid by `v`. We do this by replacing the x'th element in the y'th row
    val (y, x) = (i / 9, i % 9)

    replace(y, (r: Row) => {
      replace(x, (_: Cell) => v, r)
    }, g)
  }

  def findCell(grid: Grid) = {
    val chosenCellIndex@(c,i) = grid
      .flatten
      .zipWithIndex
      .filter{ case (c, _) => isPossible(c) }
      .sortBy{ case (c, _) =>  possibilityCount(c) }
      .head

    fixCell(i, c)
  }

  def nextGrids(grid: Grid): (Grid, Grid) = {
    val (i, first, rest) = findCell(grid)
    (replace2D(i, first, grid), replace2D(i, rest, grid))
  }

  def testNextGrids() = {
    val Some(aGrid) = readGrid(sampleGridStr2)
    showGridWithPossibilities(aGrid)

    println("\n\nAfter pruning \n\n")

    val Some(prunedGrid) = fixPoint(aGrid)
    showGridWithPossibilities(prunedGrid)

    println(s"\n\n The next cell chosen is ${findCell(prunedGrid)}")
    println(s"\n\nIs it filled? ${isGridFilled(prunedGrid)}")

    val (ng1, ng2) = nextGrids(prunedGrid)
    println("\n\nNext Grid #1")
    showGridWithPossibilities(ng1)
    println("\n\nNext Grid #2")
    showGridWithPossibilities(ng2)
  }

  def isGridFilled(grid: Grid): Boolean = {
    grid.flatten.filter(isPossible(_)).isEmpty
  }

  def isInvalidRow(row: Row): Boolean = {
    def hasDups[T](l: List[T]): Boolean = (l.size != l.distinct.size)

    val fixeds = row.collect { case Fixed(x) => x }
    val emptyPossibles = row.collect { case Possible(l) if l.isEmpty => l }

    hasDups(fixeds) || !emptyPossibles.isEmpty
  }

  def isGridInvalid(grid: Grid): Boolean = {
    grid.exists(isInvalidRow(_)) ||
    grid.transpose.exists(isInvalidRow(_)) ||
    subGridToRows(grid).exists(isInvalidRow(_))
  }

  def solve(grid: Grid): Option[Grid] = {
    for {
      pg <- fixPoint(grid)

      nextGrid <- solveHelper(pg)
    } yield nextGrid
  }

  def solveHelper(grid: Grid): Option[Grid] = {
    if (isGridInvalid(grid)) {
      None
    } else if (isGridFilled(grid)) {
      Some(grid)
    } else {
      val (g1, g2) = nextGrids(grid)
      val solvedG1 = solve(g1)
      solvedG1 match {
        case None => solve(g2)
        case _ => solvedG1
      }
    }
  }

  def testSolve() {
    val Some(aGrid) = readGrid(sampleGridStr2)
    println("\n\nInitial grid:\n")
    showGridWithPossibilities(aGrid)

    val Some(finalGrid) = solve(aGrid)
    println("\nThe result is ... ")
    showGridWithPossibilities(finalGrid)
  }

  def exclusivePossibilities(row: Row): List[List[Int]] = {
    def foldHelper1(m: Map[Int, List[Int]], entry: (Cell, Int)): Map[Int, List[Int]] =  entry match {
      case (Possible(nums), idx) =>
	nums.foldLeft(m) {  (m,n) =>
          m.updatedWith(n)( (optList) => optList match {
            case None => Some(List(idx))
            case Some(list) => Some(list.appended(idx))
          })
        }
    }

    def foldHelper2(m: Map[List[Int], List[Int]], entry: (Int, List[Int])): Map[List[Int], List[Int]] = entry match {
      case (idx, nums) =>
        m.updatedWith(nums)( (optList) => optList match {
          case None => Some(List(idx))
          case Some(list) => Some(list.appended(idx))
        })
    }

    // Get map of indices to values.
    val interim = row.
      zip(Range(1,10).toList).
      filter {
        case (c, _) => isPossible(c)
      }.
      foldLeft(Map(): Map[Int, List[Int]])(foldHelper1(_, _)).
      filter(_._2.length < 4)

    // Flip map and extract list of indices.
    interim.
      foldLeft(Map(): Map[List[Int], List[Int]])(foldHelper2(_, _)).
      filter { case (k, v) => k.length == v.length }.
      values.
      toList
  }

  def makeCell(nums: List[Int]): Option[Cell] = nums match {
    case List() => None
    case List(x) => Some(Fixed(x))
    case xs => Some(Possible(xs))
  }

  def testMakeCell() = {
    println(makeCell(List()))
    println(makeCell(Nil))
    println(makeCell(List(1)))
    println(makeCell(List(1,2)))
    println(makeCell(List(1,2,3)))
  }

  def pruneCellsByFixed(cells: List[Cell]): Option[List[Cell]] = {
    val fixeds = cells.collect { case Fixed(x) => x }

    def pruneCell(c: Cell): Option[Cell] = c match {
      case f @ Fixed(_) => Some(f)
      case Possible(nums) => makeCell(nums.filterNot(fixeds.toSet))
    }

    traverser(cells)(pruneCell)
  }

  def pruneCellsByExclusives(cells: List[Cell]): Option[List[Cell]] = {
    val exclusives = exclusivePossibilities(cells)
    val allExclusives = exclusives.flatten

    def pruneCell(c: Cell): Option[Cell] = c match {
      case f @ Fixed(_) => Some(f)
      case p @ Possible(nums) => {
        val intersection = nums.intersect(allExclusives)

        if (exclusives.contains(intersection)) {
          makeCell(intersection)
        } else {
          Some(p)
        }
      }
    }

    exclusives match {
      case List() => Some(cells)
      case _ => traverser(cells)(pruneCell)
    }
  }

  def pruneCells(cells: List[Cell]): Option[List[Cell]] = {
    for {
      step1 <- fixM[List[Cell]](pruneCellsByFixed, cells)
      step2 <- fixM[List[Cell]](pruneCellsByExclusives, step1)
    } yield step2
  }

  def testPruneCellsByFixed() = {
    val cells = List(Fixed(2), Possible(List(5, 8)), Possible(List(5, 8, 9)), Fixed(7), Possible(List(3, 4, 7)), Fixed(4), Fixed(1), Possible(List(3, 5, 7)), Possible(List(3, 5, 9)))

    val result = fixM[List[Cell]](pruneCellsByFixed, cells)
    println(s"Debug: step1 = $result")
  }

  def runTests() = {
    // testReadCell()
    // testReadRow()
    // testReadGrid()
    // testPruneCells()
    // testSubGridToRows()
    // testPruneGrid()
    // testFixPoint()
    // testFixCell()
    // testPruneCellsByFixed()
    // testNextGrids()
    testSolve()
    // testMakeCell()
  }

  // Main
  println("Sudoku-man is here ...")
  // runTests()

  if (args.length != 1) {
    println("Require a filename to read sudoku entries from")
  }

  val filename = args(0)
  for (line <- Source.fromFile(filename).getLines) {
    readGrid(line) match {
      case None => println(s"Invalid input: $line")
      case Some(aGrid) => solve(aGrid) match {
        case None => println("No solution found")
        case Some(finalGrid) => showGridWithPossibilities(finalGrid)
      }
    }
  }

}


